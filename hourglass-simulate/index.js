const puppeteer = require('puppeteer');
const os = require('os');

const l = (user, msg) => {
  console.log(`[${user}]: ${msg}`);
};

async function simulateTesting(browser, hourglassAddr, user, seconds) {
  l(user, 'Browserless connected.');
  const page = await browser.newPage();

  await page.goto(hourglassAddr);
  await page.type('#username', user);
  await page.type('#user_password', user);
  await page.click('[type=submit]');
  l(user, 'Submitted login page.');
  const exam = await page.waitForSelector('[href^="/exams/"]');
  l(user, 'Logged in.');
  await exam.click();
  l(user, 'Loading exam.');
  const button = await page.waitForXPath('//button[contains(., "Begin")]', { visible: true });
  await button.click();
  l(user, 'Started exam.');

  // wait for scratch button, click it, type
  const scratch = await page.waitForXPath('//span[contains(., "Scratch space")]/ancestor::div[1]', { visible: true });
  await scratch.click();
  l(user, 'Opened scratch space.');

  const scratchBox = await page.waitForSelector('#scratchbox');
  const val = await page.$eval('#scratchbox', box => box.value);
  await page.$eval('#scratchbox', box => box.value = '');
  l(user, `Scratch space contains [${val}].`);
  const re = /(.*): (.*)/;
  const [, username, num] = val.match(re) || [];
  if (username && user != username) {
    throw new Error(`I am NOT ${username}...`);
  }
  const text = `${user}: ${Number(num || 0) + 1}`;
  scratchBox.type(text);
  l(user, `Typed [${text}] in scratch space.`);

  await page.waitForTimeout(seconds * 1000);
  return true;
}

function sleep(milis) {
  return new Promise((resolve) => setTimeout(resolve, milis));
}

async function testOne(hourglassAddr, browserlessURL, username, seconds) {
  l(username, 'Simulating.');
  let browser;
  while (!browser) {
    try {
      browser = await puppeteer.connect({ browserWSEndpoint: browserlessURL });
    } catch (e) {
      l(username, `ERROR starting browser: ${e.message}`);
      await sleep(200);
    }
  }
  while (true) {
    try {
      const finished = await simulateTesting(browser, hourglassAddr, username, seconds);
      if (finished) {
        browser.disconnect();
        return;
      }
    } catch(e) {
      l(username, `ERROR: ${e.message}`);
      l(username, 'Waiting 2 minutes.');
      await sleep(1000 * 120);
      l(username, 'Retrying.');
    }
  }
}

async function main(hourglassAddr, browserlessURL, firstUserNum, numUsers) {
  console.log(`Starting at user ${firstUserNum}, with ${numUsers} users.`);
  const lastUserNum = firstUserNum + numUsers;
  const promises = [];
  for (i = firstUserNum; i < lastUserNum; i++) {
    const promise = testOne(hourglassAddr, browserlessURL, `stresstest${i}`, 300);
    promises.push(promise);
    await sleep(100);
  }

  await Promise.all(promises);
}

const hostname = os.hostname();
const myIndex = Number(hostname.split('-')[1]) || 0;

const [node, script, hourglassAddr, browserlessURL, numUsersStr] = process.argv;
const numUsers = Number(numUsersStr);
const firstUserNum = numUsers * myIndex;

if (process.argv.length != 5) {
  console.log(`Usage: ${node} ${script} <hourglass-url> <browserless-url> <number-of-users>`);
  process.exit(1);
}

main(hourglassAddr, browserlessURL, firstUserNum, numUsers);

// testOne('http://192.168.110.201/', 'ws://192.168.110.202', 'cs2500student', 300);
