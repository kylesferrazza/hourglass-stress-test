# hourglass-stress-test

Kubernetes-based stress testing for [Hourglass][hg].

To view browserless web UI for a particular browserless pod: `kubectl port-forward -n hourglass-stress pod/browserless-9 4000:4000`, then hit `localhost:4000` in a browser.

Reset the db with: `rails db:truncate_all; rails db:populate`

[hg]: http://github.com/codegrade/hourglass
