let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  name = "hourglass-stress-test";

  buildInputs = with pkgs; [
    nodejs
    yarn
  ];

  shellHook = ''
    export PATH=$PWD/node_modules/.bin:$PATH
  '';
}
